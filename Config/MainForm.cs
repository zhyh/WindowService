﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WindowServices.Server.Interface;

namespace WindowServices.Config
{
    public partial class MainForm : Form
    {
        ServerContext _server = new ServerContext();
        public MainForm()
        {
            InitializeComponent();
            //_service = new ServiceControllerEx("CommonPlugService");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            FreshUi.FreshUIControl(this);

            btnAddService.Enabled = false;
            btnDeleteService.Enabled = false;
            btnUpdateService.Enabled = false;

            btnStart.Enabled = false;
            btnStop.Enabled = false;
            BindGridView();
            timer1.Enabled = true;
            timer1.Start();
        }
        List<ServiceEntityEx> _list;
        private void BindGridView()
        {
            _list = _server.GetServiceList();
            dgvServices.DataSource = null;
            dgvServices.ReadOnly = true;
            dgvServices.AutoGenerateColumns = false;
            colName.DataPropertyName = "Name";
            colType.DataPropertyName = "Type";
            colType.Width = 450;
            colVersion.DataPropertyName = "Version";
            colInheritance.DataPropertyName = "Inheritance";
            dgvServices.DataSource = _list;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ServiceFrom sf = new ServiceFrom(_server);
            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                BindGridView();
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                BeginUpdate();
                if (dgvServices.SelectedRows.Count > 0)
                {
                    var l = (from DataGridViewRow dr in dgvServices.SelectedRows select _list[dr.Index]).First();
                    _server.SendCustomCommand((int)ServiceCommand.Delete, l, null);
                    _server.WaitForCommplete(300);
                    BindGridView();
                }
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.ToString());
            }
            finally
            {
                EndUpdate();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;

            btnAddService.Enabled = false;
            btnDeleteService.Enabled = false;
            btnUpdateService.Enabled = false;

            try
            {
                switch ((System.ServiceProcess.ServiceControllerStatus)(int)_server.GetServiceState())
                {
                    case System.ServiceProcess.ServiceControllerStatus.Stopped:
                        btnStart.Enabled = true;
                        btnStop.Enabled = false;
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Running:
                        btnStart.Enabled = false;
                        btnStop.Enabled = true;

                        btnAddService.Enabled = true;
                        btnDeleteService.Enabled = true;
                        btnUpdateService.Enabled = true;
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Paused:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {


            }
            finally
            {
                timer1.Enabled = true;
            }

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            btnAddService.Enabled = false;
            btnDeleteService.Enabled = false;
            btnUpdateService.Enabled = false;

            try
            {
                switch ((System.ServiceProcess.ServiceControllerStatus)(int)_server.GetServiceState())
                {
                    case System.ServiceProcess.ServiceControllerStatus.Stopped:
                        _server.Start();
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Running:
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Paused:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                timer1.Enabled = true;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;

            btnAddService.Enabled = false;
            btnDeleteService.Enabled = false;
            btnUpdateService.Enabled = false;

            try
            {
                switch ((System.ServiceProcess.ServiceControllerStatus)(int)_server.GetServiceState())
                {
                    case System.ServiceProcess.ServiceControllerStatus.Stopped:
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Running:
                        _server.Stop();
                        break;
                    case System.ServiceProcess.ServiceControllerStatus.Paused:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                timer1.Enabled = true;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            var sf = new ServiceFrom(_server);
            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                BindGridView();
            }
        }

        private void BeginUpdate()
        {
            timer1.Enabled = false;
            pnlOperate.Enabled = false;
        }

        private void EndUpdate()
        {
            timer1_Tick(null, null);
            pnlOperate.Enabled = true;
        }
    }
}
