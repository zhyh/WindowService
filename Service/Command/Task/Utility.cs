using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using WindowServices.Common;
using WindowServices.Server.Interface;
using mscoree;
using System.Linq;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command.Task
{
    internal sealed class Utility
    {
        public static void UNload(string key, ServiceHelper sh)
        {
            if (sh == null)
            {
                return;
            }

            string serviceName = null;
            try
            {
                serviceName = sh.ServiceName;
                LogHelper.WriteEntityServiceInfo(string.Format(Resources.StopServiceStart, serviceName), EventType.Info);
                var sd = new StopDelegate(sh.Stop);
                sd.BeginInvoke(null, null);
                Thread.Sleep(5000);
                sh.Dispose();
            }
            catch (System.Runtime.Remoting.RemotingException re)
            {
                LogHelper.WriteEntityServiceInfo(string.Format(Resources.StopServiceError, serviceName, re), EventType.StopServiceError);
                return;
            }
            finally
            {
                try
                {
                    AppDomain.Unload(sh.CurrentDomain);
                }
                catch (CannotUnloadAppDomainException re)
                {
                    ProcessUnLoadDomain(key, re);
                }
                catch (System.Runtime.Remoting.RemotingException re)
                {
                    ProcessUnLoadDomain(key, re);
                }
            }

            LogHelper.WriteEntityServiceInfo(string.Format(Resources.StopServiceSuccess, serviceName), EventType.Info);
        }


        private static void ProcessUnLoadDomain(string key, Exception re)
        {
            LogHelper.WriteEntityServiceInfo(string.Format(Resources.UnLoadDomainError, key, re, (re.InnerException != null ? re.InnerException.ToString() : "")), EventType.StopServiceError);

            try
            {
                LogHelper.WriteEntityServiceInfo(string.Format(Resources.FindUnLoadDomainStart, key), EventType.StopService);
                var domain = FindDomain(key);
                if (domain != null)
                {
                    AppDomain.Unload(domain);
                    LogHelper.WriteEntityServiceInfo(string.Format(Resources.FindUnLoadDomainSuccess, key), EventType.StopService);
                }
                else
                {
                    LogHelper.WriteEntityServiceInfo(string.Format(Resources.DomainNotFound, key), EventType.StopService);
                }
            }
            catch (Exception exception)
            {
                LogHelper.WriteEntityServiceInfo(string.Format(Resources.FindUnLoadDomainError, key, exception), EventType.StopServiceError);
            }
        }

        private static AppDomain FindDomain(string friendName)
        {
            return GetAppDomains().FirstOrDefault(c => c.FriendlyName == friendName);
        }

        private static IEnumerable<AppDomain> GetAppDomains()
        {

            IList<AppDomain> list = new List<AppDomain>();
            IntPtr enumHandle = IntPtr.Zero;
            ICorRuntimeHost host = new CorRuntimeHostClass();
            try
            {
                host.EnumDomains(out enumHandle);
                while (true)
                {
                    object domain;
                    host.NextDomain(enumHandle, out domain);
                    if (domain == null)
                    {
                        break;
                    }

                    list.Add((AppDomain)domain);
                }

                return list;

            }
            catch (Exception exception)
            {
                LogHelper.WriteEntityServiceInfo(string.Format("查询应用程序域出错.\n错误信息：{0}", exception), EventType.StopServiceError);
            }

            finally
            {
                host.CloseEnum(enumHandle);
                Marshal.ReleaseComObject(host);
            }

            return list;
        }


        delegate void StopDelegate();
        public static ServiceHelper LoadService(ServiceEntityEx se)
        {
            LogHelper.WriteEntityServiceInfo(string.Format(Resources.LoadServiceStart,se.Name), EventType.Info);

            try
            {
                var sh = ServiceHelper.CreateServiceHelper(se);
                if (sh != null)
                {

                    sh.Initialize(se);
                    sh.Start();
                    LogHelper.WriteEntityServiceInfo(string.Format(Resources.LoadServiceSuccess,se.Name), EventType.Info);
                    return sh;
                }

                LogHelper.WriteEntityServiceError(string.Format(Resources.LoadServiceError, se.Name), EventType.CreateServiceError);
            }
            catch (Exception ee)
            {
                LogHelper.WriteEntityServiceError(ee.ToString(), EventType.StartServiceError);
            }

            return null;
        }

        public static void DeleteServiceDir(ServiceEntityEx se)
        {
            if (!Directory.Exists(se.BasePath))
            {
                return;
            }

            try
            {
                LogHelper.WriteEntityServiceError(string.Format(Resources.DeletePlugDirectoryStart, se.BasePath), EventType.Info);
                Directory.Delete(se.BasePath, true);
                LogHelper.WriteEntityServiceError(string.Format(Resources.DeletePlugDirectorySuccess, se.BasePath), EventType.Info);
            }
            catch (Exception exception)
            {
                LogHelper.WriteEntityServiceError(string.Format(Resources.DeletePlugDirectoryError, se.BasePath, exception), EventType.DeletePlugPathError);
            }
        }

        public static void UnZipFile(ServiceEntityEx se)
        {
            if (!File.Exists(se.ZipFileName))
            {
                throw new Exception(string.Format(Resources.ZipFileNotFound, se.ZipFileName));
            }

            if (Directory.Exists(se.BasePath))
            {
                Directory.Delete(se.BasePath,true);
            }

            Directory.CreateDirectory(se.BasePath);

            if (!ZipHelper.UnZip(se.ZipFileName, se.BasePath + "\\", null))
            {
                throw new Exception(string.Format(Resources.UnZipFileError, se.ZipFileName,""));
            }
        }
    }
}