namespace WindowServices.Server.Interface
{
    /// <summary>
    /// 自定义服务命令
    /// </summary>
    public enum ServiceCommand
    {
        /// <summary>
        /// 新增服务
        /// </summary>
        Add = 128,
        /// <summary>
        /// 删除服务
        /// </summary>
        Delete = 129,
        /// <summary>
        /// 更新服务
        /// </summary>
        Update = 130,
    }
}