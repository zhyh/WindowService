using System;

namespace WindowServices.Server.Interface
{
    [Serializable]
    public class User
    {
        public string Domain { get; set; }
        public string UserId { get; set; }
        public string PassWord { get; set; }
    }
}