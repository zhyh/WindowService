﻿using System;
using System.Diagnostics;
using WindowServices.Common.Properties;

namespace WindowServices.Common
{
    public enum EventType
    {
        Start = 20,
        Started = 21,
        Stop = 22,
        Stoped = 23,
        StopService = 23,
        StopServiceEnd = 24,
        StartService = 25,
        StartServiceEnd = 26,
        StopServiceError = 27,
        StartServiceError = 28,
        ReadConfigError = 29,
        WriteconfigError = 30,
        DeletePlugPathError = 31,
        Info = 32,
        CreateServiceError = 33,
        CreateService = 34,
        CreateServiceEnd = 35,
        RunServiceError = 36,
    }

    public class LogHelper
    {
        public static void WriteEntity(string source, string message, EventType eventid, EventLogEntryType type)
        {
            try
            {
                EventLog.WriteEntry(source, message, type, (int) eventid);
            }
            catch (Exception exception)
            {
                LogServiceHelper.LogHelper.Logger.FatalFormat(Resources.WriteLogToOSEventError, exception);
            }

            switch (type)
            {
                case EventLogEntryType.Error:
                    LogServiceHelper.LogHelper.Logger.ErrorFormat(Resources.LogTemplate, source, message, eventid);
                    break;
                case EventLogEntryType.FailureAudit:
                    LogServiceHelper.LogHelper.Logger.FatalFormat(Resources.LogTemplate, source, message, eventid);
                    break;
                case EventLogEntryType.Information:
                    LogServiceHelper.LogHelper.Logger.InfoFormat(Resources.LogTemplate, source, message, eventid);
                    break;
                case EventLogEntryType.SuccessAudit:
                    LogServiceHelper.LogHelper.Logger.InfoFormat(Resources.LogTemplate, source, message, eventid);
                    break;
                case EventLogEntryType.Warning:
                    LogServiceHelper.LogHelper.Logger.WarnFormat(Resources.LogTemplate, source, message, eventid);
                    break;
            }
        }

        public static void WriteEntityService(string message, EventType eventid, EventLogEntryType type)
        {
            WriteEntity("Service", message, eventid, type);
        }

        public static void WriteEntityServiceInfo(string message, EventType eventid)
        {
            WriteEntityService(message, eventid, EventLogEntryType.Information);
        }

        public static void WriteEntityServiceError(string message, EventType eventid)
        {
            WriteEntityService(message, eventid, EventLogEntryType.Error);
        }

        public static void WriteEntityServiceWarning(string message, EventType eventid)
        {
            WriteEntityService(message, eventid, EventLogEntryType.Warning);
        }

        public static void WriteEntityConfig(string message, EventType eventid, EventLogEntryType type)
        {
            WriteEntity("Config", message, eventid, type);
        }

        public static void WriteEntityConfigInfo(string message, EventType eventid)
        {
            WriteEntityConfig(message, eventid, EventLogEntryType.Information);
        }

        public static void WriteEntityConfigError(string message, EventType eventid)
        {
            WriteEntityConfig(message, eventid, EventLogEntryType.Error);
        }

        public static void WriteEntityConfigWarning(string message, EventType eventid)
        {
            WriteEntityConfig(message, eventid, EventLogEntryType.Warning);
        }
    }
}